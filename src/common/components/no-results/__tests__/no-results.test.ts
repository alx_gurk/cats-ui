import { test, expect } from '@playwright/test';

const text = 'Тут текст ошибки, который я хочу передать';

test.skip('Отображается надпись ошибки', async ({ page }) => {
  await page.goto('/iframe.html?args=&id=noresults--default&viewMode=story');

  await expect(page.locator('//img[@src="/img/weary-cat.png"]')).toBeVisible();
  await expect(page.getByText(text)).toBeVisible();
});
