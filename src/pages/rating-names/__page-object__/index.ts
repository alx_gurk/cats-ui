import type { Page, TestFixture } from '@playwright/test';
import { test, expect } from '@playwright/test';
import { errorResponse } from '../__mocks__/error-response';

// Определение класса CatRatingPage
export class CatRatingPage {
  private page: Page;

  // Конструктор класса CatRatingPage
  constructor({ page }: { page: Page }) {
      this.page = page;
  }

  // Метод для открытия страницы рейтинга котиков
  async openCatRatingPage() {
      return await test.step('Открыть страницу рейтинга котиков', async () => {
          await this.page.goto('https://meowle.fintech-qa.ru/rating');
          console.log('Открыта страница рейтинга котиков');
      });
  }

  // Метод для извлечения количества лайков всех котиков
  async extractLikes() {
      return await test.step('Извлечь значения количества лайков всех котиков', async () => {
          const counts = await this.page.evaluate(() => {
              const elements = Array.from(document.querySelectorAll('td.has-text-success'));
              const likes = elements.map(e => parseInt(e.textContent));
              console.log('Значения количества лайков котиков:', likes);
              return likes;
          });
          return counts;
      });
  }

  // Метод для проверки порядка отображения лайков по убыванию
  async checkLikeOrder(likes: number[]) {
      return await test.step('Проверить порядок отображения лайков по убыванию', async () => {
          for (let i = 0; i < likes.length - 1; i++) {
              console.log(`Сравнение ${likes[i]} и ${likes[i + 1]}`);
              expect(likes[i]).toBeGreaterThanOrEqual(likes[i + 1]);
          }
      });
  }

  // Метод для имитации ошибочного ответа
  async mockErrorResponse() {
      await this.page.route('**/likes/cats/rating', async (route) => {
          await route.fulfill({
              status: 500,
              contentType: 'application/json',
              body: JSON.stringify(errorResponse),
          });
      });
  }

  // Метод для проверки отображения уведомления об ошибке
  async checkErrorNotification(errorMessage: string) {
      await test.step('Проверить отображение уведомления об ошибке', async () => {
          const errorTextElement = await this.page.waitForSelector('div[class*="ajs-visible"]', { timeout: 60000 });
          const errorText = await errorTextElement.innerText();
          console.log('Текст ошибки:', errorText);
          expect(errorText).toBe(errorMessage);
      });
  }
}

// Тип и фикстура для CatRatingPage
export type CatRatingPageFixture = TestFixture<CatRatingPage, { page: Page }>;

// Фикстура для создания экземпляра CatRatingPage и передачи его в функцию use
export const catRatingPageFixture: CatRatingPageFixture = async ({ page }, use) => {
  const catRatingPage = new CatRatingPage({ page });
  await use(catRatingPage);
};

