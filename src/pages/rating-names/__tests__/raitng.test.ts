import { errorResponse } from '../__mocks__/error-response';
import { test, expect } from '@playwright/test';


test.skip('Задание 1. При ошибке сервера в методе raiting - отображается попап ошибки (1 способ)', async ({ page }) => {
  // Перехватываем запрос, который содержит /likes/cats/rating, и возвращаем ошибку с кодом 500
  await page.route(
    request => request.href.includes('/likes/cats/rating'),
    async route => {
      await route.fulfill({
        status: 500,
        contentType: 'application/json',
        body: JSON.stringify(errorResponse),
      });
    }
  ); 

  // Переходим на страницу c рейтинком котиков
  await page.goto('https://meowle.fintech-qa.ru/rating');
  console.log('Открыта страница рейтинга котиков');

  // Получаем текстовое содержимое всей страницы
  const pageTextContent = await page.textContent('html');

  // Проверяем, содержит ли текстовое содержимое ожидаемый текст
  expect(pageTextContent).toContain('Ошибка загрузки рейтинга');
});


test.skip('Задание 1. При ошибке сервера в методе raiting - отображается попап ошибки (2 способ)', async ({ page }) => {
  // Перехватываем запрос, который содержит /likes/cats/rating , и возвращаем ошибку с кодом 500
  await page.route(
    request => request.href.includes('/likes/cats/rating'),
    async route => {
      await route.fulfill({
        status: 500,
        contentType: 'application/json',
        body: JSON.stringify(errorResponse),
      });
    }
  ); 

  // Переходим на страницу c рейтинком котиков
  await page.goto('https://meowle.fintech-qa.ru/rating');
  console.log('Открыта страница рейтинга котиков');

  // Находим элемент с классом ajs-visible локатор XPath
  const errorElement = await page.locator('//div[contains(@class,"ajs-visible")]');

  // Получаем текст элемента
  const errorText = await errorElement.textContent();

  // Проверяем, содержит ли текст элемента ожидаемый текст
  expect(errorText).toContain('Ошибка загрузки рейтинга');
});

test('Задание 1. При ошибке сервера в методе raiting - отображается попап ошибки (3 способ)', async ({ page }) => {
  // Перехватываем запрос, который содержит /likes/cats/rating , и возвращаем ошибку с кодом 500
  await page.route(
    request => request.href.includes('/likes/cats/rating'),
    async route => {
      await route.fulfill({
        status: 500,
        contentType: 'application/json',
        body: JSON.stringify(errorResponse),
      });
    }
  ); 

  // Переходим на страницу c рейтинком котиков
  await page.goto('https://meowle.fintech-qa.ru/rating');
  console.log('Открыта страница рейтинга котиков');
  
  // Ждем CSS-селектор с классом ajs-visible
  const errorTextElement = await page.waitForSelector('div[class*="ajs-visible"]', { timeout: 60000 });

  const errorText = await errorTextElement.innerText();
  expect(errorText).toBe('Ошибка загрузки рейтинга');
});


test.skip('Рейтинг котиков отображается лайки по убыванию', async ({ page }) => {
  // Переходим на страницу c рейтинком котиков
  await page.goto('https://meowle.fintech-qa.ru/rating');
  console.log('Открыта страница рейтинга котиков');

  // Извлекаем значения количества лайков элементов XPath
  const counts = await page.evaluate(() => {
    const elements = Array.from(document.querySelectorAll('td.has-text-success'));
    const likes = elements.map(e => parseInt(e.textContent));
    console.log('Значения количества лайков:', likes);
    return likes;
  });

  // Проверяем, что рейтинг количества лайков отображается по убыванию
  for (let i = 0; i < counts.length - 1; i++) {
    console.log(`Сравнение ${counts[i]} и ${counts[i + 1]}`);
    expect(counts[i]).toBeGreaterThanOrEqual(counts[i + 1]);
  }
});
