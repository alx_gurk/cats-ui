import { test as base } from '@playwright/test'; 
import { CatRatingPage, catRatingPageFixture } from '../__page-object__/index';


// Определение расширенного теста с объектом страницы catRatingPage
const test = base.extend<{ catRatingPage: CatRatingPage }>({ 
  catRatingPage: catRatingPageFixture, // Использование фикстуры для страницы catRatingPage
});

test.skip('Рейтинг котиков отображается лайки по убыванию', async ({ catRatingPage }) => { 
    console.log('Шаг 1: Открыть страницу рейтинга котиков');
    await catRatingPage.openCatRatingPage();
    
    console.log('Шаг 2: Извлечь значения количества лайков элементов');
    const likes = await catRatingPage.extractLikes(); 
    
    console.log('Шаг 3: Проверить порядок отображения лайков по убыванию');
    await catRatingPage.checkLikeOrder(likes); 
});


test.skip('Проверка отображения уведомления об ошибке на странице рейтинга котиков', async ({ catRatingPage }) => {
  await catRatingPage.mockErrorResponse();

  console.log('Шаг 1: Открыть страницу рейтинга котиков');
  await catRatingPage.openCatRatingPage();

  console.log('Шаг 2: Проверить отображение уведомления об ошибке');
  await catRatingPage.checkErrorNotification('Ошибка загрузки рейтинга');
});



  
