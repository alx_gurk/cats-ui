import { test, expect } from '@playwright/test';

test.skip('После ввода имени в строку поиска кнопка поиска - кликабельная', async ({ page   }) => {
  await page.goto('/');
  await page.fill('//input[@placeholder]', 'Петр');
  await expect(page.locator('//button[@type="submit"]')).toBeEnabled()
});