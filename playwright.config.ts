import { devices } from '@playwright/test';

/**
 * В целом дока pw https://playwright.dev/docs
 * Параметры конфига https://playwright.dev/docs/test-configuration.
 */
const config = {
  testMatch:  process.env.STAND ? /src\/pages\/[^\/]+\/__tests__\/.*\.test\.ts/ :  /.*\/components\/.*\/__tests__\/.*\.test\.ts/,

  /**
   * Про репортеры https://playwright.dev/docs/test-reporters
   */
  reporter: [
    ['html', { open: 'never' }],
    ['allure-playwright', { outputFolder: 'allure-results' }],
  ],

  use: {
    baseURL: process.env.STAND ? process.env.STAND : 'http://localhost:6006',
    /**
     * Про таймауты https://playwright.dev/docs/test-timeouts
     */
    navigationTimeout: 30_000,


    /**
     * Трейс тестов https://playwright.dev/docs/trace-viewer
     */
    trace: 'retain-on-failure',
  },

  expect: {
    timeout: 10_000,
  },

  projects: [
    {
      name: 'chromium',
      use: { ...devices['Desktop Chrome'] },
    },
    {
      name: 'webkit',
      use: { ...devices['Desktop Safari'] },
    },
  ],
};

export default config